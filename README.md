# IN-WOP project

## Roadmap

```mermaid
%%{init: { 'gitGraph': { 'mainBranchName': 'seinebasin2'}} }%%

gitGraph
    checkout seinebasin2
    commit id:"sb2 nov 2022" tag:"v0.2.0"
    branch rvgest
    checkout rvgest
    commit id:"rvg nov2022" tag:"v0.1.0"
    branch irmara
    checkout irmara
    commit id: "irm nov2022" tag: "v0.3.0"
    checkout rvgest
    commit id:"Hydratec" tag: "v0.2.0"
    checkout seinebasin2
    commit id: "Qnat v1" tag: "v0.2.1"
    checkout rvgest
    merge seinebasin2
    checkout seinebasin2
    commit id: "Qnat v2" tag: "v0.3.0"
    checkout rvgest
    merge seinebasin2
    commit id: "Qnat GCM/RCM" tag: "v0.3.0"
    checkout irmara
    merge rvgest
    commit id: "Select periods" tag: "v0.4.0"
    checkout seinebasin2
    commit id: "Current rules" tag: "v0.4.0"
    checkout rvgest
    commit id: "derive rules" tag: "v0.4.0"
    checkout seinebasin2
    merge rvgest
    commit id: "New rules" tag: "v0.5.0"
    checkout irmara
    merge rvgest
    commit id: "Realtime Decision" tag: "v0.5.0"

```
