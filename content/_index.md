---
title: "WP3: Seine River case study"
author: "David Dorchies"
date: 2023-03-16
cascade:
  featured_image: '/images/aube_lake.jpg'
---

The third Work Package of the project "Seine River case study" is led by the
French partners of the project:

- **Inrae** (mixed research unit G-EAU at Montpellier)
- the engineering & project management company **Artelia**.

# General organisation of the tools used in IN-WOP WP3

The case study is based on several tools developed during the project :

* An extension of the R package **[airGR](https://hydrogr.github.io/airGR/)** called
**[airGRiwrm](https://airgriwrm.g-eau.fr)**, which allows to integrate watermanagement
in a hydrological semi-distributed model
* An integrated semi-distributed hydrological model called
**[seinebasin2](https://in-wop.pages.mia.inra.fr/seinebasin2)** based on the R package **airGRiwrm**
* An mono-objective optimisation tool for multi-reservoir operation
**[VGEST](https://forgemia.inra.fr/in-wop/vgest)**
* The R package **[rvgest](https://in-wop.pages.mia.inra.fr/rvgest)**, a suite of tools
to compute optimised decision
derived from risk assessment computed by **VGEST**,
* And **[IRMaRA](http://irmara.g-eau.net)** (Interactive Reservoir MAnagement
Risk Assessment), a graphical interface which allows to interactively explore
risk assessment and derived decision

## Roadmap of tool interactions

<pre class="mermaid">
%%{init: { 'gitGraph': { 'mainBranchName': 'seinebasin2'}} }%%

gitGraph
    checkout seinebasin2
    commit id:"sb2 nov 2022" tag:"v0.2.0"
    branch rvgest
    checkout rvgest
    commit id:"rvg nov2022" tag:"v0.1.0"
    branch irmara
    checkout irmara
    commit id: "irm nov2022" tag: "v0.3.0"
    checkout rvgest
    commit id:"Hydratec" tag: "v0.2.0"
    checkout seinebasin2
    commit id: "Qnat v1" tag: "v0.2.1"
    checkout rvgest
    merge seinebasin2
    checkout seinebasin2
    commit id: "Qnat v2" tag: "v0.3.0"
    checkout rvgest
    merge seinebasin2
    commit id: "Qnat GCM/RCM" tag: "v0.3.0"
    checkout irmara
    merge rvgest
    commit id: "Select periods" tag: "v0.4.0"
    checkout seinebasin2
    commit id: "Current rules" tag: "v0.4.0"
    checkout rvgest
    commit id: "derive rules" tag: "v0.4.0"
    checkout seinebasin2
    merge rvgest
    commit id: "New rules" tag: "v0.5.0"
    checkout irmara
    merge rvgest
    commit id: "Realtime Decision" tag: "v0.5.0"
</pre>

<script type="module">
    import mermaid from 'https://cdn.jsdelivr.net/npm/mermaid@10/dist/mermaid.esm.min.mjs';
    mermaid.initialize({ startOnLoad: true });
</script>

# Acknowledgements

IN-WOP is a research project funded by the European Commission and the French
National Research Agency (ANR) for funding in the frame of the
collaborative international consortium
[IN-WOP](http://www.waterjpi.eu/joint-calls/joint-call-2018-waterworks-2017/booklet/in-wop)
financed under the 2018 Joint call of the WaterWorks2017 ERA-NET Cofund.
This ERA-NET is an integral part of the activities developed by the
Water JPI.

<div style="display: flex; justify-content: space-between;">
<img src="logo_water_jpi.png" alt="Water JPI"> <img src="logo_water_works_2017.png" alt="Water Works2017"> <img src="logo_european_commission.jpg" alt="EuropeanCommission"> <img src="logo_2018_joint_call.png" alt="2018 Jointcall">
</div>
